import cv2
import numpy as np 
from IRTracker import irt5, Track

print "import CSV and time"
import csv
import time
import os
from app import ffVideoWriter
#import thread
from  Tkinter import Tk
from tkFileDialog import askdirectory

hashchars='0123456789abcdefghijklmnopqrstuvwxyz'
# using only lowercase for convenience and avoid collision in Windows
# hashchars='01' #for binary

def epochToHash(n):
  '''
  monotonic timestamp hash. directly from Scott Harden's website
  '''
  hash=''
  while n>0:
    hash = hashchars[int(n % len(hashchars))] + hash
    n = int(n / len(hashchars))
  return hash
  

	
class guiPanel(object):
	
	def panelEvents(self,event,x,y,flags,param):
		tracker=self.tracker
		button=self.button
		if event in [cv2.EVENT_LBUTTONDBLCLK, cv2.EVENT_LBUTTONUP]:
			active=[]
			for key in button:
				y0,y1,x0,x1 =button[key]['coords']
				if x in range(x0,x1) and y in range(y0,y1): 
					active.append(key)
			for name in active:
				if name in self.router:
					self.router[name](name)
			
	def __init__(self):
		
		self.pause=True
		
		self.cont=True
		self.record=True
		sprites=cv2.imread("sprites.png")
		
		self.dirname="../output/"
		self.panel=panel=np.zeros((sprites.shape[0], 512,3), np.uint8)
		for c in range(panel.shape[1]):
			panel[:,c]=sprites[:,-2]
		self.button=button={}
		for n, name in enumerate(['disk', 'pause', 'start', 'record', 'eject', 'stop', 'clear', 'exit', 'shield']):
			button[name]= {'sprite':sprites[:64,n*64:n*64+64], 'coords': [-1,-1,-1,-1]}
			
		for n, name in enumerate([ 'Blue','Yellow','Orange', 'Green', 'Red']):
			button[name]={'sprite':sprites[64:128, n*64+64:n*64+128], 'coords':[ -1,-1,-1,-1]}
			
		y0,y1=0,64
		x0=2
		for key in [ 'shield','disk', 'start', 'record', 'pause','clear','stop', 'exit']:
			if key in ['record', 'eject']:
				button[key]['coords'] = button['start']['coords']
			else:
				x1=x0+64
				button[key]['coords'] = y0,y1,x0,x0+64 # button[key]['sprite'].shape[1]
				panel[y0:y1, x0:x1] = button[key]['sprite']
				x0+=66 #   button[key]['sprite'].shape[1] + xpadding
				
		cv2.imshow("panel", panel)
		self.tracker=irt5(None, windowName='unknown')
		tracker=self.tracker
		tracker.draw_str(panel, ( 5,80), "(c) 2015 University of Chicago / OPRI")
		tracker.draw_str(panel, ( 5,100), "P.I.: Dr Alexander Langerman, Programmer: Philippe Grassia")
		tracker.draw_str(panel, ( 5,118), "License : GPL V.2 https://bitbucket.org/opri/irtracker.git")
		tracker.draw_str(panel, ( 5,136), "Icons: Tango Project http://tango.freedesktop.org/ ")
		
			
		cv2.setMouseCallback('panel',self.panelEvents)
		self.router={
			'disk':self.onSaveAs, 
			'pause':self.onPause, 
			'start':self.onStart, 
			'record':self.onRecord,
			'eject': self.notImplemented, 
			'shield': self.notImplemented, 
			'stop':self.onStop,
			'clear':self.onClear, 
			'exit':self.onStop,
			'Blue': self.onColor,
			'Yellow': self.onColor,
			'Orange': self.onColor,
			'Green': self.onColor, 
			'Red': self.onColor
		}
		
		
	def onClear(self, param):
		tracker=self.tracker	
		for tr in tracker.tracks:
			tr.heatmap*=0
			tr.trajectory*=0
			
	def onSaveAs(self,param):
		Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
		dirname = askdirectory(initialdir=self.dirname, title='IRTracker: choose file to save recording') 
		if len(dirname.strip())==0: 
			 tkMessageBox.showwarning(
            "Recording location",
            "Until you select a valid folder to save the data, nothing will be recorded"
        )
		self.dirname=dirname
		
		
		
	def onPause(self,param):
		self.pause= not self.pause
		if self.pause:
			self.showIcon("start", 0.5)
		else:
			self.showIcon("record")
		
		
		
	def onStart(self,param):
		self.onSaveAs("start")
		self.pause=False
		self.cont=True
		self.record=True
		self.tracker.firstFrame()
		
		
	def onRecord(self,param):		
		if not os.path.isdir(self.dirname):
			self.onSaveAs("record")
			dirname=self.dirname
			if len(dirname.strip())==0: 
				record=False
				self.showIcon("start")
				return
		tracker=self.tracker
		tracker.csvFile= open("{0}/trajectories.csv".format(self.dirname), "wb")
		tracker.csvWriter= csv.writer(tracker.csvFile, quoting=csv.QUOTE_MINIMAL)
		self.vdoWriter=ffVideoWriter()
		self.record=True
		self.showIcon("record")
		
		
			
	def notImplemented(self,param):
		print "not implemented **yet**"
		pass 
		
	def onStop(self,param):	
		self.cont=False
		self.record=False
		self.showIcon("start")
		if hasattr(self, "vdoWriter"):
			self.vdroWriter.release()
		
	def onColor(self, param):
		print "toggle color", param	
		pass

	def showIcon(self, iconName="", dimFactor=1.):
		Y0,Y1,X0,X1=self.button[iconName]['coords']
		sprite=np.uint8(self.button[iconName]['sprite']*dimFactor)
		self.panel[Y0:Y1,X0:X1]=sprite

	def mainLoop(self):
		tracker=self.tracker	
		windowName=tracker.windowName
		n=10
		while self.cont: 
			cv2.imshow("panel", self.panel)
			if n<=0:
				n=500
			n-=1
			if self.pause:
				cv2.waitKey(1)
				continue
			try:
				frame=tracker.cap.readframe()
			except OPRIReaderException:  # please do not catch everything and hide other stuff.
				print "OpriReader exception, reached the end of file, exiting"
				self.onStop("exception")
			if frame is None:
				print "frame is None, exiting"
				self.onStop("ended")
			tracker.frameNumber+=1
			result=tracker.process(frame) 
			if result is None:
				pass 
				self.onStop("could not process")
			tracker.draw_str(result, ( 5,20), tracker.timestamp())
			if self.record:
				if not hasattr(self.vdoWriter, "pipe"): #vdoWriter not start
					self.vdoWriter.start("{0}/tracjectories.mp4".format(self.dirname), 
									(result.shape[1], result.shape[0]))
					print "started videoWriter"
				self.vdoWriter.write(result)
				cv2.imwrite("{0}/trajectories.png".format(self.dirname), result)
				bluePrint=self.tracker.bluePrint
				for tr in self.tracker.tracks:
					heat= cv2.addWeighted(bluePrint, 1,tr.coloredHeatmap(),1 ,0)
					cv2.imwrite("{0}/{1}.png".format(self.dirname, tr.name), heat)
			cv2.imshow(windowName,result)
			cv2.waitKey(1)
			
		


if __name__== "__main__":
	
	
	panel=guiPanel()
	panel.tracker.firstFrame()
	
	while panel.cont:
		panel.mainLoop()
	
