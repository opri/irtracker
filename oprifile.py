#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  oprifile.py
#  
#  Copyright 2014 OPRI University of Chicago Medicine http://opri.uchicago.edu
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


import numpy as np
import struct
import math
import cv2
import os
import gzip 
from glob import glob

import time
		
		
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
import ctypes
import comtypes

class OPRIReaderException(Exception):
	str=None
	def __init__(self, str= None):
		if str is None:
			self.str='Not enough bytes in file to satisfy read request'
		else:
			self.str=str
			
	def __str__(self):
		return self.str


class OpriReader(object):
	fps=0
	file = None 
	skel = None
	numberOfChannel=0
	channelDescription= []
	currentFrame= dict()
	typeNames = {
		'int8'   :'b',
		'uint8'  :'B',
		'int16'  :'h',
		'uint16' :'H',
		'int32'  :'i',
		'uint32' :'I',
		'int64'  :'q',
		'uint64' :'Q',
		'float'  :'f',
		'double' :'d',
		'char'   :'s'}
	myDtype= {1:'B', 2:'H', 4: '4B',
		'CO': '4B', 'IF':'H','DP':'H','BD':'B', 'BM':'f', 'DG': 'B', 'TS':'q'}
	currentFrame = None
	frameSize=0
	
	def __init__(self, fileName):
		self.numberOfChannel=0
		self.filename=fileName
		self.channelDescription= []
		# handle both gzip compressed and uncompressed files 
		if fileName[-3:]=='.gz':
			self.file = gzip.open(fileName, 'r')
		else:
			self.file = open(fileName, 'rb')
		OPRI=self.file.read(4)
		if 'OPRI' != OPRI:
			raise  OPRIReaderException('Not an OPRI file')
		try:
			self.skel=open(fileName+'.skel')
			self.skel.readline()
			self.params=self.skel.readline().split(',')
			print self.params
			if self.fps <> int(self.params[1]):
				raise OPRIReaderException('opri and skel have different fps')
		except IOError:
			self.skel=None
	
		filecolor=fileName.replace('.opri','.mp4')
		if filecolor.endswith('.gz'): filecolor=filecolor[:-3]
		self.color=cv2.VideoCapture(filecolor)
		ret,img= self.color.read()
		# a quite brutal rewind...
		self.color.release()
		if not ret:
			self.color=None
		else:
			self.color=cv2.VideoCapture(filecolor)
		self.fps= self.read('uint8')
			
			
		self.numberOfChannel= self.read('uint8')
		print "Number of Channels: ", self.numberOfChannel
		self.frameSize=0
		for i in xrange(self.numberOfChannel):
			fieldType=self.file.read(2)
			print "channel {0}: [{1}]".format(i,fieldType)
			#if not fieldType in self.myDtype:
			#	raise OPRIReaderException("unknown channel type [{0}]".format(fieldType))
			if fieldType in self.myDtype:
				fieldWidth=self.read('uint16')
				fieldHeight= self.read('uint16')
				fieldBitsPerPixel=self.read('uint8')
			else:  # work around a header bug fixed(?) on 2014.10.10 
				print "header workaround for BM"
				fieldType='BM'
				fieldWidth=2
				fieldHeight=1
				fieldBitsPerPixel=32
				self.file.seek(-2, os.SEEK_CUR)	
			bytesperpixel=int( 1+ math.floor((fieldBitsPerPixel-1)/8))
			pixelsperframe= fieldWidth*fieldHeight
			self.frameSize+=pixelsperframe*bytesperpixel
			self.channelDescription.append({'fieldType':fieldType,
								'width':fieldWidth,
								'height':fieldHeight,
								'bitsperpixel': fieldBitsPerPixel,
								'bytesperpixel': bytesperpixel, 
								'pixelsperframe': pixelsperframe}   ) 
			if 'DG' == fieldType: print self.channelDescription[-1]

	def read(self, typeName):
		typeFormat = self.typeNames[typeName.lower()]
		typeSize = struct.calcsize(typeFormat)
		value = self.file.read(typeSize)
		if typeSize != len(value):
			raise BinaryReaderEOFException
		return struct.unpack(typeFormat, value)[0]
		
	def readframe(self):
		frames=[]
		fieldDescription=self.channelDescription
		curr= dict()
		fullFrame=self.file.read(self.frameSize)
		try:
			ret, img=self.color.read()
			if ret: curr['PG']=img
		except AttributeError:
			pass
		if len(fullFrame)<self.frameSize:
			raise OPRIReaderException('incomplete frame1')
		channelStart=0
		for  field in fieldDescription:
			frame=np.fromstring(fullFrame[channelStart:], self.myDtype[field['fieldType']], count=field['pixelsperframe'])
			channelStart+=field['pixelsperframe']* field['bytesperpixel']
			if field['pixelsperframe']==len(frame):
				frame=frame.reshape((field['height'], field['width'],-1))
				curr[field['fieldType']]=frame
			else: 
				raise OPRIReaderException('incomplete frame')
		self.currentFrame= curr
		return curr
		
	def __del__(self):
		self.file.close()

	def getComboBlue(self, frame=None):
		if frame is None:
			frame=self.currentFrame
		R=np.uint8(frame['DP'] / 32)
		G=np.uint8(frame['DP'])
		if 'BD' in frame:
			B=frame['BD']
		else:
			B=np.ones(R.shape)*255
		return np.dstack((B,G,R))

	def getComboRed(self, frame=None):
		if frame is None:
			frame=self.currentFrame
		B=np.uint8(frame['DP'] / 32)
		G=np.uint8(frame['DP'])
		if 'BD' in frame:
			R=frame['BD']
		else:
			R=np.ones(B.shape)*255
		return np.dstack((B,G,R))



class KinectReader(object):
	def __init__(self, channels=None):
		if channels is None:
			channels= PyKinectV2.FrameSourceTypes_Infrared | PyKinectV2.FrameSourceTypes_Depth | PyKinectV2.FrameSourceTypes_Color 
		# Kinect runtime object, we want depth, infrared and color
		self._kinect = PyKinectRuntime.PyKinectRuntime(channels)
		self.channels=channels
		print self._kinect
		getter={}
		if channels & PyKinectV2.FrameSourceTypes_Infrared:
			getter['IF']=self._kinect.get_last_infrared_frame
			
		if channels & PyKinectV2.FrameSourceTypes_Depth:
			getter['DP']=self._kinect.get_last_depth_frame
		
		if channels & PyKinectV2.FrameSourceTypes_Color:
			getter['CO']=self._kinect.get_last_color_frame
	
		# 	
		if channels & PyKinectV2.FrameSourceTypes_BodyIndex:
			getter['BD']=self._kinect.get_last_body_index_frame		

		# this one is a bit of a misnommer: it's the skeletal data
		if channels & PyKinectV2.FrameSourceTypes_Body:
			getter['SK']=self._kinect.get_last_body_frame
		self.getter=getter
		self.DSP=None

	def __del__(self):
		self._kinect.close()		

	def readframe(self):
		has=0
		curr={'TS': int(time.clock()*1000)}
		kinect=self._kinect   # stanza is a bit long in the tooth, let's
		channels=self.channels
		
		muche=int(time.clock()*797*511) # ! just a stoopid pseudo RNG
		
		if channels & PyKinectV2.FrameSourceTypes_Color:
			data= kinect.get_last_color_frame().reshape(kinect.color_frame_desc.Height,kinect.color_frame_desc.Width,4)
			data=data[...,:3]	# eqv to cvtColor(data, cv2.COLOR_BGRA2BGR) but faster	
			curr['CO']=data
			curr['PG']=cv2.resize(data, (640,360), interpolation=cv2.INTER_AREA)
		if channels & PyKinectV2.FrameSourceTypes_Depth:
			curr['DP']=kinect.get_last_depth_frame().reshape(kinect.depth_frame_desc.Height,kinect.depth_frame_desc.Width)
		if channels & PyKinectV2.FrameSourceTypes_Infrared:
			curr['IF']=kinect.get_last_infrared_frame().reshape(kinect.infrared_frame_desc.Height,kinect.infrared_frame_desc.Width)
		if channels & PyKinectV2.FrameSourceTypes_BodyIndex:
			curr['BD']=kinect.get_last_body_index_frame().reshape(kinect.body_index_frame_desc.Height,kinect.body_index_frame_desc.Width)

		sav=curr['PG'][0,0]
		curr['PG'][0,0]=(0,0,0)

		if (channels & PyKinectV2.FrameSourceTypes_Color) and (channels & PyKinectV2.FrameSourceTypes_Depth):
			
			npCSP=np.int0(kinect.map_depth_to_color()/3)
			npDSP=np.int0(cv2.resize(kinect.map_color_to_depth(),(640,360)))
			curr['depthMapped']=curr['PG'][npCSP[...,1].clip(0,359), npCSP[...,0].clip(0,639)]
			curr['colorMapped']=curr['DP'][npDSP[...,1].clip(0,423), npDSP[...,0].clip(0,511)]
			if 'IF' in curr:
				curr['infraMapped']=curr['IF'][npDSP[...,1].clip(0,423), npDSP[...,0].clip(0,511)]

		curr['PG'][0,0]=sav
		
		return curr
				


def readOrCalculateHistogram(filename):
	histFile=filename+".npy"
	sampleFile= filename+".png"
	try:
		roihist=np.load(histFile)
		print "loaded precomputed {0} histogram".format(filename)
		
	except:
		sample=cv2.imread(sampleFile)
		sample=sample.reshape(-1,3)
		print "could not load precalculated {0} histogram".format(filename)
		size= sample.shape[0]
		mask=np.float32(sample[:,:]!=(0,0,255))
		redRemoved =[x for x in sample if x[0]>0 or x[1]>0 or x[2]<250]
		
		redRemoved=np.array(redRemoved).reshape(-1,1,3)
		
		sample=cv2.cvtColor(redRemoved,cv2.COLOR_BGR2HSV)
		# calculating object histogram
		roihist = cv2.calcHist([redRemoved],[0, 1], None, [180, 256], [0, 180, 0, 256] )
		# normalize histogram 
		cv2.normalize(roihist,roihist,0,255,cv2.NORM_MINMAX)
		np.save(histFile, roihist)
	return roihist
	
	
def readOrCalculateRGBHistogram(filename):
	histFile=filename+"RGB.npy"
	sampleFile= filename+".png"
	try:
		roihist=np.load(histFile)
		print "loaded precomputed {0} histogram".format(filename)
		
	except:
		sample=cv2.imread(sampleFile)
		sample=sample.reshape(-1,3)
		print "could not load precalculated {0} histogram".format(filename)
		size= sample.shape[0]
		mask=np.float32(sample[:,:]!=(0,0,255))
		redRemoved =[x for x in sample if x[0]>0 or x[1]>0 or x[2]<250]
		
		redRemoved=np.array(redRemoved).reshape(-1,1,3)
		
		sample=cv2.cvtColor(redRemoved,cv2.COLOR_BGR2HSV)
		# calculating object histogram
		roihist = cv2.calcHist([redRemoved],[0, 1, 2], None, [256, 256, 256], [0, 256, 0, 256, 0, 256] )
		# normalize histogram 
		cv2.normalize(roihist,roihist,0,255,cv2.NORM_MINMAX)
		np.save(histFile, roihist)
	return roihist
	
def main():
	k2=KinectReader()
	
	while time.clock()<600:
		d= k2.readframe()
		print time.clock()
		for k,img in enumerate(d):
			name="img{0}".format(k)
			if img in ['BD','PG']:
				print d[img].shape ,
				cv2.imshow(name, d[img])
			if img in ['DP', 'IF']:
				s=240* np.ones_like(d[img], np.uint8)
				v=s
				h=np.uint8(130.*d[img]/d[img].max())
					
				cv2.imshow(name, cv2.cvtColor(np.dstack((h,s,v)), cv2.COLOR_HSV2BGR))
				
			#full size color image ('CO'), body and audio not handled here
			if img=='depthMapped':
				cv2.imshow("dM", d[img])
		cv2.waitKey(10)
	cv2.waitKey(5000)
	return 0

def test():
	import gzip
	
	
	assert cv2.__version__> '2.4.7' or cv2.__version__>='2.4.10' or cv2.__version__>="3"
	print cv2.__version__
	
	#oprilist = glob("*.opri.gz") + glob.glob('*.opri')
	#oprilist += glob("*/*.opri") 
	#oprilist += glob("../kinect-data/*/*.opri")
	oprilist = glob('C:/Users/Philippe/Documents/kinect-data/20141002\proc2/*.opri.gz')
	#oprilist = glob('../kinect-data/opri-*/*.opri.gz')
	maskImg=np.ones((424,512), np.uint16)*4095
				
	#oprilist=glob.glob("kinect2-movie1*.opri")
	for oprifile in oprilist:
		opr=OpriReader(oprifile)
		print "-"*20
		print oprifile
		print opr.fps
		print opr.channelDescription
		print "="*20
		nFrames=0
		ret=True
		pause=10
		previousIndex=0
		while ret:
			try:
				frm=opr.readframe()
			except OPRIReaderException:  # please do not catch everything and hide other stuff.
				ret=False
			else:
				if 'IF' in frm:
					hist=frm['IF'].sum(axis=0)
					index= hist.argmin()
					if index>previousIndex:
						print "{0:5d}  shift at pixel: {2}".format(nFrames,hist.shape, index, hist[index])
						#previousIndex=index
						opr.file.seek(2*(index), os.SEEK_CUR)
					if frm['DP'].max()<8000:
						cv2.imshow("IF", cv2.applyColorMap(np.uint8(255*np.exp(-384.0/(1+frm['IF']))), cv2.COLORMAP_HOT))
					else:
						cv2.imshow("IF", frm['IF'])
					if nFrames%20==0:
						print "{3:5d}: IF {0}/{1}/{2}".format(frm['IF'].max(),frm['IF'].mean(), frm['IF'].std(), nFrames)
				if frm['DP'].max()>8000:
					#frm['DP']=cv2.bitwise_and(frm['DP'], maskImg)
					frm['DP']=cv2.normalize(frm['DP'], alpha=0, beta=7800, norm_type=cv2.NORM_MINMAX)
				if 'CO' in frm:
					cv2.imshow('color', frm['CO'])
				cv2.imshow("combo red", opr.getComboBlue(frm))
				cv2.imshow("scaled jet", cv2.applyColorMap(np.uint8(255*np.exp(-1000.0/(1+frm['DP']))), cv2.COLORMAP_JET))
				cv2.imshow("scaled", cv2.applyColorMap(np.uint8(frm['DP']/31), cv2.COLORMAP_HOT))
				if 'BD' in frm:
					cv2.imshow("silhouette", frm['BD'])
				if 'DG' in frm and nFrames % 25==0:
					#print chr(frm['DG'][0])+chr(frm['DG'][1])
					print np.int0(frm['DG'][2:])
					
			key = cv2.waitKey(pause) % 255
			if key == 27:
				ret=False
			nFrames+=1

			if nFrames% 100 ==0 and nFrames>0:
				print nFrames, frm['DP'].mean(), frm['DP'].max()
		del opr
		print "end of ", oprifile
		cv2.waitKey(0)

if __name__ == '__main__':
	main()
	exit()
	test()





class App:
	moduloFrames=1
	def __init__(self,source=0, windowName= None, fps=-1):
		if   isinstance(source, str):
			self.cap=OpriReader(source)
			self.oprifile=source
		elif isinstance(source,(OpriReader, KinectReader)):
			self.cap=source
			if isinstance(source, KinectReader):
				self.oprifile="KinectV2"
			else:
				self.oprifile=source.filename
		self.pause=1
		self.disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))	
		if windowName is None:
			windowName= os.path.basename(str(oprifile))
		self.windowName=windowName
		self.frameNumber=0
		self.fps=self.cap.fps
		height, width= 424,512
		self.disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))	
		self.XYZ=np.zeros((height, width,3), np.float32)
		self.xy=np.zeros((424,512,2), np.float32)
		for x in range(width):
			self.XYZ[:,x,0]=(x-256.)/8000.
			self.xy[:,x,0]= x
		for y in range(height):
			self.XYZ[y,:,1]= (y-212.)/8000.
			self.xy[y,:,1]= y
		self.XYZ[...,2]=1./20.	
		
	
	def timestamp(self):
		ms=float(self.frameNumber/self.fps)
		hh=int(ms/3600)
		mm=int(ms/60) % 60
		ss=int(ms) %60
		return "{0:3d}:{1:02d}:{2:02d}".format(hh,mm,ss)

	
	def draw_str(self, dst, (x, y), s):
		cv2.putText(dst, s, (x+1, y+1), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 0), thickness = 2, lineType=cv2.CV_AA)
		cv2.putText(dst, s, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), lineType=cv2.CV_AA)

	
	def firstFrame(self):
		'''
		input none:
		output: none:
		destined to be overloaded: takes care of all inits that might be 
		necessary at the first frame time, after the video source opens
		'''
		
	def rectIntersect(rect1, rect2):	
		'''
		rect1 (x,y,w,h), rect2 (x,y,w,h)
		'''
		x1,y1,w1,h1=rect1
		x2,y2,w2,h2=rect2
		DX=abs(2*x1+w1-2*x2-w2)
		DY=abs(2*y1+h1-2*y2-h2)
		return DX<w1+w2 and DY<h1+h2
		 
	def depthTotheMap(self, cx,cy,cz):
		'''
		retro projects the depth map data to theMap world
		should be 
		X= ScaleX*cx*cz+ originX
		Y= ScaleY*cy*cz+ originY 
		'''
		return [int((cx-256.0)*cz/8000.0), int((cy-212.0)*cz/8000.0), int(cz/20)]
	
			
	def depthToPCL(self, depthMap):
		# use cv2.cvtColor so speedup the conversion from 1 to 3 channels
		return self.XYZ* cv2.cvtColor(np.float32(depthMap), cv2.COLOR_GRAY2BGR)	
	
		
	def process(self, frame):
		'''
		input :frame current frame as a numpy array 
		return: frame with processed content/visualisation
		destined to be overloaded: 
		'''
		return frame
		
		
		
	def preConditionFrame(self , frame):
		'''
		input frame
		output preconditionned frame
		convenience function apply initial filtering (denoise, blur, rescale, cvt color)
		to ease the processing
		'''
		return cv2.flip(frame['DP'], 1)
				
	def run(self):
		'''
		input none
		output none
		main loop of the app
		'''
		if not hasattr(self ,"actionRouter"):
			self.actionRouter={}
		try:
			print "restart at frame:", self.frameNumber
		except AttributeError:
			self.frameNumber=0
		windowName=self.windowName
		self.firstFrame()
		self.cont=True
		router = { 27: self.OnExit, 81: self.OnExit, 113:self.OnExit,
					}
		while self.cont:
			try:
				frame=self.cap.readframe()
			except OPRIReaderException:  # please do not catch everything and hide other stuff.
				print "OpriReader exception, reached the end of file, exiting"
				break
			if frame is None:
				print "frame is None, exiting"
				break
			self.frameNumber+=1
			result=self.process(frame) 
			if result is None: 
				break
				
			self.draw_str(result, ( 5,20), self.timestamp())
			if self.frameNumber % self.moduloFrames ==0:
				cv2.imshow(windowName,result)
				key= chr(cv2.waitKey(self.pause)%256)
				
				if key in ['q','Q', chr(27)]:
					break
				elif key in ['p','P',' ']:
					self.pause=0
				elif key in ['f','F']:
					self.pause=10
				elif key in ['S','s']:
					self.pause=5000
				if key in self.actionRouter:
					self.actionRouter[key](key)
					
		cv2.destroyAllWindows();

	def OnExit(self, *arg, **kwarg):
		self.cont=False
		
		

