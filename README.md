# IRTracker

Pure Python circulation tracker for kinectv2. Uses colored retroreflective tags to follow mobiles


## Dependencies:
- Windows 8+ with kinect v2 sdk 
- Opri's version of pykinectv2 provided with the projects (wraps Infrared capture and corrdinate mapping, not provided by Microsoft at the time of writing) 
- Python 2.7.9+ (as the time of writing incompatible with Python 3) 
- tested in 32 bits, untested 64 bits python, should work though
- opencv 2.4.9+ (opencv 3 should work)
- numpy 

- hidden dependency : zmq (used for multiple networked kinects in an other project) 


## Caveats and recommendations

So far the palette is somewhat limited : Red and Orange tend to overlap and conflict, Yellow, Green, Blue. 

provision has been made for turquoise and purple: needs to be better characterized though. 

Can use retroreflective tape colored or white with filter/permanent marker ... 
Use at least 2"x3" tags, otherwise it will be too small to register at more than approx 15 ft. 

It is assumed that all tags of the same color are attached to 1 single mobile/individual


