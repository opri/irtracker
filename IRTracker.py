#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  IRTracker.py
#  
#  Copyright 2015 University of Chicago/OPRI http://opri.uchicago.edu/
#    
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from oprifile import App, OpriReader, OPRIReaderException, KinectReader

import cv2
import numpy as np 
import datetime

from glob import glob
import ctypes
import os
import sys	
	
import threading 
import time	

from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime

import csv

#kinectDepthCalibration
k1=0.09265684 #secondOrder
k2=-0.2690772 #fourthOrder
k3=0.09464066 #sixthOrder
p1=0 # no tangential distorsion
p2=0 
D=np.array([k1,k2,p1,p2,k3])
PrincipalPoint=np.array([261.702,211.0505])
FocalLength=np.array([364.805,364.805])
CameraMatrix=np.array([	[FocalLength[0], 0., PrincipalPoint[0]],
						[0., FocalLength[1], PrincipalPoint[1]],
						[0.,			 0.,		1]])


newCamera, roi = cv2.getOptimalNewCameraMatrix(CameraMatrix, D, (512,424), 0) 



hashchars='0123456789abcdefghijklmnopqrstuvwxyz'
# using only lowercase for convenience and avoid collision in Windows
# hashchars='01' #for binary


def epochToHash(n):
  '''
  monotonic timestamp hash. directly from Scott Harden's website
  '''
  hash=''
  while n>0:
    hash = hashchars[int(n % len(hashchars))] + hash
    n = int(n / len(hashchars))
  return hash
  


def sobelSilhouette(grey, kernelSize=3):
	gx= cv2.Sobel(grey, cv2.CV_32F, 1,0,kernelSize)			
	gy= cv2.Sobel(grey, cv2.CV_32F, 0,1,kernelSize)
	mag, ang = cv2.cartToPolar(gx, gy)
	return cv2.convertScaleAbs(mag)

class BackgroundSubtractorRAVG(object):
	def __init__(self):
		self.bg=None
	
	def apply(self, img, learningRate=0.01):
		if self.bg is None:
			self.bg=img.copy()
		else:
			self.bg= self.bg*(1-learningRate)+img*learningRate
		delta=cv2.absdiff(np.float32(img), np.float32(self.bg))
		try:
			return cv2.inRange(delta.max(axis=2), 64, 65536)
		except ValueError:
			return cv2.inRange(delta, 64,65536)


class BackgroundSubtractorMHI(object):
	def __init__(self):
		self.bg=None
	
	def apply(self, img, learningRate=0.01):
		if self.bg is None:
			self.bg=img.copy()
			self.mhi=  np.zeros(img.shape[:2],np.float32)
			return np.zeros(img.shape[:2],np.uint8)
		delta=255*np.uint8(cv2.absdiff(self.bg, img)> 20)
		self.bg=img.copy()
		self.mhi=self.mhi.clip(10,255)-10
		cv2.updateMotionHistory(delta, self.mhi, 255, 250)
		return cv2.erode(cv2.inRange(self.mhi, 50,256), None)

	
class Track(object):
	def __init__(self, name, colorRange, drawingColor):
		self.name = name
		self.hist=[]
		tmp=np.zeros((180,256), np.float32)
		tmp[colorRange, 12:]=64. if name=='BLUE' else 255. 
		self.hist=cv2.GaussianBlur(tmp, (5,5),0)
		self.points=[]
		self.color=drawingColor
		self.heatmap = np.zeros((424,424), np.uint32)
		self.trajectory = np.zeros((424,424), np.uint8)
		#lowColor= drawingColor.clip(64,192)
		lowColor=drawingColor/4
		if name=='RED':
			lowColor=np.uint8((64,64,128))
		self.colorRange= np.zeros((257,3), np.uint8)
		for i in range(1,256):
			p1=i/255.
			p2=(255.-i)/255.
			self.colorRange[i]=np.uint8(lowColor*p2+drawingColor*p1)	
		self.colorRange[0]=[0,0,0]
		self.colorRange[256]=drawingColor
		self.disc=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (8,8)).reshape(8,8)
		self.disc=np.ones((8,8), np.uint8)
		self.disc[0]=0
		self.disc[:,0]=0
		
		
	def updateHeatmap(self, x,y):
		#self.heatmap[y-3:y+4,x-3:x+4]+=self.disc
		span=self.disc.shape[0]
		x=span*int(x/span)
		y=span*int(y/span)
		self.heatmap[y:y+span, x:x+span]+=self.disc
		#self.heatmap=cv2.dilate(self.heatmap, None)
		#self.heatmap=cv2.erode(self.heatmap,None)
		
	
	
	def coloredHeatmap(self):
		vis=np.uint8(self.heatmap*255./max(1,self.heatmap.max()))
		return self.colorRange[vis]	
		
	def updateTrajectory(self, p,p0):
		cv2.line(self.trajectory, (p[0], p[2]), (p0[0],p0[2]), 1)
		
		
	def coloredTrajectory(self):
		LUT=np.uint8([[0,0,0],self.color]).reshape(2,3)
		return LUT[self.trajectory]
			
	def line(self, PrincipalPoint, FocalLength):
		if len(self.points)==0:
			return None
		pts= np.array(self.points)
		for ch in [0,1]:
			pts[...,ch]=(pts[...,ch]-PrincipalPoint[ch])/FocalLength[ch]*pts[...,2]
		return pts

	def broken_trajectory(self,PrincipalPoint, FocalLength, threshold=500):
		if len(self.points)==0:
			return None
		pts= np.array(self.points)
		for ch in [0,1]:
			pts[...,ch]=(pts[...,ch]-PrincipalPoint[ch])/FocalLength[ch]*pts[...,2]
		pt1=pts[1:]
		pt2=pts[:-1]
		dist = np.append(np.sqrt(((pt1-pt2)**2).sum(axis=1)),0)
		traj=[]
		tmp=[]
		for i,d in enumerate(dist):
			if d<threshold:
				tmp.append(pts[i])
			else:
				if len(tmp)>1:
					traj[-1].append(np.array(tmp))
					traj.append([])
				tmp=[pts[i]]
				
		if self.name=="YELLOW":
			print self.name, dist, traj
		return traj
		

class irt5(App):	
	def __init__(self,source=None, windowName= None, fps=-1):
		
		self.HashStamp=epochToHash(int(time.time()))
		
		if isinstance(source, (KinectReader, OpriReader)):
			self.cap=source
			try:
				self.oprifile=source.oprifile
			except AttributeError:
				self.oprifile="PyKinectV2"
				
		if source is None:			
			self.cap=KinectReader(	PyKinectV2.FrameSourceTypes_Depth | 
									PyKinectV2.FrameSourceTypes_Infrared |
									PyKinectV2.FrameSourceTypes_Color |
									PyKinectV2.FrameSourceTypes_BodyIndex
								)
			self.oprifile="PyKinectv2"
			
		if isinstance(source, str):
			self.cap=OpriReader(source)
			self.oprifile=source
			
		if isinstance(self.cap, KinectReader):
			self.kinect=self.cap._kinect
		else:
			self.kinect=PyKinectRuntime.PyKinectRuntime(	
									PyKinectV2.FrameSourceTypes_Depth | 
									PyKinectV2.FrameSourceTypes_Infrared |
									PyKinectV2.FrameSourceTypes_Color |
									PyKinectV2.FrameSourceTypes_BodyIndex
								)	
		self.c_ushort_p=ctypes.POINTER(ctypes.c_ushort)						
		self.pause=1
		self.disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))	
		if windowName is None:
			windowName= os.path.basename(str(oprifile))
		self.windowName=windowName
		
		if not hasattr(self, "frameNumber"): self.frameNumber=0
		
		try:
			self.fps=self.cap.fps
		except AttributeError:
			self.fps=8
			
		height, width= 424,512
		self.disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))	
		self.fgbg=cv2.BackgroundSubtractorMOG2()
		self.actionRouter= { 'd': self.debug, 'D': self.debug, 
					'r' : self.reset, 'R': self.reset, 
					's' : self.saveProfiles, 'S': self.saveProfiles}
					
	def __del__(self):
		try: 
			self.cap.close()
		except AttributeError:
			pass
		self.vdoWriter.release()
		del self.vdoWriter
		del self.cap 
		self.csvWriter.close()
		self.csvFile.close()
		
			
	def reset(self, key=None):
		self.prev=np.zeros_like(self.prev)
		self.floorPlan*=0
		
		for i in range(len(self.tracks)):
			self.tracks[i].points=[]
		print "reset of map and trajectories"
	
	def saveProfiles(self, key):
		self.HSVMeasured[:,:64]=1
		hsvMean=self.HSVMeasured[self.HSVMeasured>2].mean()
		hsv= np.uint8(self.HSVMeasured> hsvMean/10)*255
		crcbMean=self.CRCB[self.CRCB>2].mean()
		crcb= np.uint8(self.CRCB> crcbMean/10)*255
		labMean=self.Lab[self.Lab>2].mean()
		lab= np.uint8(self.Lab> labMean/10)*255
		crcb[0]=255
		lab[0]=255
		snap=np.vstack((hsv,crcb,lab))
		cv2.imshow("hsv,crcb, lab profiles", snap)
		cv2.imwrite("profiles.png", snap)
		
	def debug(self, key):
		frm=self.cap.readframe()
		if 'IF' not in frm:
			print "No infrared capture"
			return
		if frm['IF'].max()==0:
			print "infrared frame is empty. Is the computer fully compatible ?"
			return
		print "Lint check seems to pass"
			
	def firstFrame(self):
		height, width= 424,512
		self.colorRange=[	['YELLOW',range(15,40)], 
							#['ORANGE', range(4,19)], 
							#['BLUE', range(98,120)], 
							['RED', range(165,180)+range(0,12)], #roll over from 180 to 0
							#['GREEN',range(50,75),], 
							#['TURQUOISE',range(85,95)], 
							#['PURPLE', range(145,155)],
						]
		self.drawingColor={ 'YELLOW':np.int32((0,255,255)),
							'ORANGE': np.int32((0,165,255)), 
							'BLUE':np.int32((255, 0,0)),
							'RED':np.int32((0,0,255)),
							'GREEN':np.int32((0,255,0)),
							'TURQUOISE':np.int32((255,255,0)),
							'PURPLE':np.int32((255,0,255)),
							'WHITE': np.int32((255,255,255)),
							'BLACK': np.int32((0,0,0)),
						}	

		try: 
			if self.tracks is None:
				self.tracks=[ Track(color, rng, self.drawingColor[color]) for  color, rng in self.colorRange]
		except AttributeError:
			self.tracks=[ Track(color, rng, self.drawingColor[color]) for  color, rng in self.colorRange]
		
		self.lookup=np.ones((256,256), np.uint8)*255
		self.palette= np.zeros((256,3), np.uint8)
		for label, (name, rng) in enumerate(self.colorRange):
			self.tracks[label].hist=np.zeros((256,256), np.uint8)
			self.tracks[label].hist[rng, 16:200]=255
			self.lookup[rng, 48:]=label
			self.palette[label]=self.drawingColor[name]
		
		#blue_yellow=cv2.imread('blue-yellow.png',1)[:256]
		#pal=cv2.addWeighted(self.palette[self.lookup], 0.5, blue_yellow, 0.5,0)
		#cv2.imshow('lookup', pal)
		
		self.labels=np.zeros((360,640), np.uint8)-1
		self.disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))	
		self.XYZ=np.zeros((height, width,3), np.float32)
		self.xy=np.zeros((424,512,2), np.float32)
		for x in range(width):
			self.XYZ[:,x,0]=(x-256.)/8000.
			self.xy[:,x,0]= x
		for y in range(height):
			self.XYZ[y,:,1]= (y-212.)/8000.
			self.xy[y,:,1]= y
		self.XYZ[...,2]=1./20.
		lab=np.zeros((256,256,3), np.uint8)
		for x in range(256):
			for y in range(256):
				lab[y,x]=(200,y,x)
		self.labGammut=cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)	
		#cv2.imshow('Lab Gammut @L=160', self.labGammut)
		
		#cv2.namedWindow("cd")
		#cv2.setMouseCallback("cd", self.showDepth)
		
	def showDepth(self, event, x, y, flags, param):
		print "depth at {0},{1}: {2}".format(x,y, self.dp[y,x])
		
		
		
	def calcFloorPlan(self, pcl,dp,cd,bd, persist=True):
		try:
			# decay the existing image
			# this limits cast "shadows" visibility (if we were starting from empty frame)
			# and eventually removes tracks in usually unmeasured space
			#
			assert self.floorPlan.dtype==np.int32
			self.floorPlan-=1
			self.floorPlan=self.floorPlan.clip(0,1000)
			# Use the pixels from the bottom up to row 64 :
			# this ensures better visibility of furniture (e.g. OR table)
			# and avoids hiding everything under the color of the ceiling. 
			Y=self.xy[423:63:-1, self.frameNumber %3::3,1].astype(int).ravel()
			X=self.xy[64:, self.frameNumber %3::3,0].astype(int).ravel()
		except AttributeError:
			self.floorPlan=np.zeros((424,424,3), np.int32)
			#first frame with a few random pixels
			X=np.random.choice(512,500)
			Y=np.random.choice(424,500)
			
		drawable=(pcl[Y,X,1]>0) # only draw measured pixels
		coords=pcl[Y,X]
		coords=coords[drawable].astype(int)+(212,0,10)
		#get the matching color of the selected pixels 
		colors=cd[Y,X]
		
		# condition x and y to :
		# 1/ avoid out of bounds exceptions
		# 2/ orient the plan in a more "natural" way
		x=( coords[...,0]) % 424
		y=(-coords[...,2]) % 424
		if persist:
			self.floorPlan[y,x]=colors[drawable]
			return self.floorPlan.astype(np.uint8)
		else:
			floorPlan=np.zeros((424,424,3), np.uint8)
			floorPlan[y,x]=colors[drawable]
			return floorPlan
	
	def calcBluePrint(self,pcl, dp, co, bd):
		def sobelSilhouette(grey, kernelSize=3):
			gx= cv2.Sobel(grey, cv2.CV_32F, 1,0,kernelSize)			
			gy= cv2.Sobel(grey, cv2.CV_32F, 0,1,kernelSize)
			mag, ang = cv2.cartToPolar(gx, gy)
			return cv2.convertScaleAbs(mag)
		
		grey=cv2.GaussianBlur(cv2.cvtColor(co,cv2.COLOR_BGR2GRAY), (0,0), .6)
		
		grey[0,0]=0
		npCSP=self.npCSP
		
		sobel=sobelSilhouette(grey)
		mappedSobel=sobel[npCSP[...,1].clip(0,359), npCSP[...,0].clip(0,639)]
		mappedSobel[:32]=0
		mappedSobel[-32:]=0
		mappedSobel=cv2.dilate(mappedSobel, None)
		mappedSobel=np.maximum(mappedSobel, cv2.inRange(bd, 0,10))
		#mappedSobel=cv2.inRange(mappedSobel, 64,256)
		
		return self.calcFloorPlan(pcl, dp, cv2.cvtColor(mappedSobel,cv2.COLOR_GRAY2BGR), bd, False)
	
	def process(self, frame):
		try:
			ir=frame['IF'].clip(0,65530)
			dp=frame['DP']
			bd=frame['BD']
			co=frame['PG']
			self.dp=dp
		except KeyError:
			return None
		
		# find bright spots
		bright=cv2.inRange(ir,65530,70000)
		#bright=cv2.dilate( bright, None)
		# if visible the ledge of the table/shelf/cabinet top where the kinect sits
		# tends to mess up the measurements (usually in the yellow color)
		# so let's assume there is no marker of interest that low in the scene
		bright[350:]=0
		
		contours, hierarchy = cv2.findContours(bright.copy(),cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
		# from there patch the depth map where the bright spots prevented measurement of depth
		for n, cnt in enumerate(contours):
			#if too small forget and move on			
			if len(cnt)<3:
				continue
			xRng=range(max(0, min(cnt[...,0]-1)),min(511, max(cnt[...,0]+1)))
			yRng=range(max(0, min(cnt[...,1]-1)),min(511, max(cnt[...,1]+1)))
			raw_depth=dp[min(yRng):max(yRng), min(xRng):max(xRng)]	
			
			# originally removed non measured points only 
			# now removing points that are too close b/c the are bright 
			# regardless of the type of surface
			finite_nonzero= (raw_depth>1000)
			
			raw_depth=raw_depth[finite_nonzero]
			# if there is nothing to interpolate let's just move on.
			if len(raw_depth)<10:
				continue
			# near dumb interpolation technique
			# assign the mean value of the measured points in the bounding box
			# to the point that were not measured.
					
			interpolatedValue=int(raw_depth.mean())
			for y in yRng:
				for x in xRng:
					if dp[y,x]<1:
						dp[y,x]=interpolatedValue
					elif dp[y,x]>1000:
						interpolatedValue=dp[y,x]
		
		# when items are very close to the kinect they are so bright from 
		# laser illumination that they make false positives for the reflective badges
		# lets remove what is measured too close (less than 1 m ~ 3ft) 
		tooclose=cv2.inRange(dp, 0, 1000)
		bright[tooclose>0]=0
		
		#cv2.imshow("ir", cv2.addWeighted(cv2.normalize(np.log(1+ir), None, 0,255,cv2.NORM_MINMAX).astype(np.uint8), 0.5,
		#				bright, 0.5,0))
					
		# use the interpolated depth map to do what we want 
		dp2=cv2.dilate(dp, None) # just close the holes a little
		npCSP=np.int0(self.kinect.map_depth_to_color(dp2.ctypes.data_as(self.c_ushort_p))/3)
		self.npCSP=npCSP	
		
		# here project the color image to the depth space to find the color 
		# of the bright marker and estimate the position of the wearer
		co[0,0]=0
		
		#remap the color Image to the depthmap space
		cd=co[npCSP[...,1].clip(0,359), npCSP[...,0].clip(0,639)]
		#cv2.imshow('cd', bright)	
		hls=cv2.cvtColor(cd, cv2.COLOR_BGR2HLS)
		if cv2.countNonZero(bright)>3:
			try:
				for h,s,l in hls[bright>127]:
					self.HLSMeasured[h-1:h+1,l-1:l+1]+=1
				#self.HLSMeasured[:,:24]=1
				lHLS=np.sqrt(1+self.HLSMeasured)
				bY=cv2.cvtColor(np.uint8(lHLS/(1+lHLS.max())*255), cv2.COLOR_GRAY2BGR)
				pal=cv2.addWeighted(self.palette[self.lookup], 0.5, bY, 0.5,0)
			except AttributeError as e:
				self.HLSMeasured=np.zeros((256,256), np.uint0)
		
		resp=[ cv2.bitwise_and(tr.hist[hls[...,0],hls[...,1]], bright) for tr in self.tracks[:4]]
		pcl = self.depthToPCL(dp)
		ts={}
		for label, rp in enumerate(resp):
			key=self.colorRange[label][0]
			if cv2.countNonZero(rp)<20:
				ts[key]=[-1,-1,-1]
				continue
			hits= pcl[rp>0]
			position= pcl[rp>0].mean(axis=0)
			ts[key]=list(position)		
			self.tracks[label].points.append(position)
			
			try:
				self.csvWriter.writerow([self.frameNumber,self.tracks[label].name]+list(position))
			except AttributeError:
				self.csvFile= open("../output/{0}.csv".format(self.HashStamp), "wb")
				self.csvWriter= csv.writer(self.csvFile, quoting=csv.QUOTE_MINIMAL)
				self.csvWriter.writerow([self.frameNumber,self.tracks[label].name]+list(position))
				
				
		
		
		vis= self.calcFloorPlan(pcl, dp, cd, bd)
		bluePrint= self.calcBluePrint(pcl, dp, co, bd)
		#self.floorPlan=vis.copy().astype(np.int32)
		self.bluePrint=bluePrint.copy()
		
		#draw the sprites:
		'''
		#snippet from calcFloorPlan 
		#instead of pixels with a non zeros depth 
		# we want to redraw the pixels that changed (foreground extraction?)
		 
		sprite=(pcl[Y,X,1]>0) 
		coords=pcl[Y,X]
		coords=coords[drawable].astype(int)+(212,0,10)
		colors=cd[Y,X]
		
		x=( coords[...,0]) % 424
		y=(-coords[...,2]) % 424
		self.floorPlan[y,x]=colors[drawable]
		'''
		 
		 
		# 20150807 do not draw the trajectories that way 
		'''
		#draw the trajectories
		for tr in self.tracks:
			col=tr.color
			if len(tr.points)<3:
				continue
			points=np.int32(tr.points)+(212,0,10)
			points[...,2]=424-points[...,2]
			#points=points[-200:]
			p0=points[0]
			for p in points[1:]:
				dist=(p0[0]-p[0])**2+(p0[2]-p[2])**2
				if dist<900:
					cv2.line(vis,(p0[0],p0[2]), (p[0],p[2]), (128,128,128), 2, lineType=cv2.CV_AA)					
					cv2.line(vis,(p0[0],p0[2]), (p[0],p[2]), col, lineType=cv2.CV_AA)
				p0=p
		'''		
		# update heatmaps for each track
		N=0
		for tr in self.tracks:
			col=tr.color
			N+=1
			if len(tr.points)<2:
				continue
			points=np.int32(tr.points[-2:])+(212,0,10)
			points[...,2]=412-points[...,2]
			segment=np.zeros((424,424), np.uint8)
			p0,p=points
			dist=(p0[0]-p[0])**2+(p0[2]-p[2])**2
			if dist<900:
				#cv2.line(segment,(p0[0],p0[2]), (p[0],p[2]),(5,), 4)
				#tr.heatmap+=cv2.GaussianBlur(segment, (5,5),0)
				tr.updateHeatmap(p0[0],p0[2])
				tr.updateTrajectory(p,p0)
				
				if self.frameNumber % 10==0:
					pass
					#cv2.imwrite("../output/{2}-{0}-{1:06d}.png".format(tr.name, self.frameNumber,self.HashStamp),cv2.applyColorMap(heatVis, cv2.COLORMAP_JET))
			#heatMap=cv2.normalize(np.sqrt(tr.heatmap), None, 0,1, cv2.NORM_MINMAX).astype(np.float32)
			#heatMap=np.log(1+tr.heatmap.astype(np.float32)/tr.heatmap.max())
			#heatVis=tr.colorRange[np.int0(heatMap*256)]
			heatVis=tr.coloredHeatmap()
			cv2.imshow(tr.name, np.maximum(bluePrint,heatVis))
			vis=cv2.addWeighted(vis, 1., tr.coloredTrajectory(), 1., 0.)
				
				
		#self.floorPlan #cv2.bitwise_and(falseColors, np.dstack((bright, bright, bright)))
		colorVis=cv2.cvtColor(cv2.normalize(np.log(1+ir), None, 0,192,cv2.NORM_MINMAX).astype(np.uint8), cv2.COLOR_GRAY2BGR)
		colorVis[bright>127]=cd[bright>127]
		try:
			colorVis[:90,:128]= cv2.resize(pal[:180,:256,:], (128,90))
		except NameError:
			pass
		returned= np.hstack((colorVis, vis))
		
		
		try: 
			self.vdoWriter.write(returned)
		except AttributeError:
			pass	
		#cv2.imwrite("../output/{0}-{1:06d}.png".format(self.HashStamp,self.frameNumber),returned)
		
		return returned

		
def main():
	return 0
	
	

def oldMain():
		
	main()
	clips=[]
	# some pre recorded clips
	clips+= glob("../../circulation/ORTESTS/*10.?7*.opri.gz") +glob('../colorTracker/*.opri.gz')
	clips+= glob("../../circulation/OPRITESTS/*.opri.gz") 
	clips+= sorted(glob("../../circulation/ORTESTS/*movie11*.opri.gz")) 
	# None for : use connected kinect if any (should work with kinect studio in playback mode.
	clips= glob("../../circulation/ORTESTS/K2/kinect2-movie12*.opri.gz")+glob("../../circulation/ORTESTS/K2/kinect2-movie01*.opri.gz") #+glob('../colorTracker/*.opri.gz')
	clips.append(None)
	for n, fn in enumerate(clips):
		irt=irt5(fn, windowName='{0}-{1}'.format( "unknown", n+1))
		try: 
			irt.SOM=SOM
		except NameError:
			pass
		irt.windowName= '{0}-{1}'.format(irt.__class__.__name__, n+1)
		
		irt.pause=2
		irt.run()
		try:
			SOM=irt.SOM
		except:
			pass
		del irt


		

		
if __name__ == '__main__':
	oldMain()
